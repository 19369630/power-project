# power-grid
---
## Online Version
---
Click on '`Source`' on the left, then `static` folder from the directory and then download `sample.csv` as you will need to upload this file to run demo.  
https://vast-brushlands-74510.herokuapp.com/ 
  

  
  
----
## Local Setup 1
----

- Download the repository by clicking '`Downloads`' on the left, then '`Download repository`'  
Click '`Overview`' after downloading to return to this page

- Unzip/extract the file to your Desktop, or somewhere easy to find  
Click `Show extracted files when complete`

- Download node.js from https://nodejs.org  (LTS version)  
Run the node installer once downloaded using default options 

- Go back to the extracted folder and run `install.bat` file  
- Once that window finishes, then run `server.bat` file and leave the terminal window open

- Navigate to https://localhost:3000/
  
    
  
    
---
## Local Setup 2
---
- Download and extract the repository as above.
- Download and install node.js as above.

- Open a Command Prompt/PowerShell/Terminal window
    - Windows Key + R, then type either `cmd` or `powershell`

- Navigate to the extracted repository
    - `cd path/to/folder` to change directory
    - `dir` to view directory

Then run the following in the terminal window:
``` bash
# install dependencies
$ npm install

# serve at localhost:3000
$ npm run dev
```
Leave the window open and navigate to  
https://localhost:3000/

The required .csv file is located at `<ExtractedFolder>/static/sample.csv` 
